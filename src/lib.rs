extern crate proc_macro;

use darling::{FromMeta, FromDeriveInput};
use syn::{parse_macro_input, AttributeArgs, DeriveInput};
use quote::quote;
use proc_macro::TokenStream;

use glsl::parser::Parse;
use glsl::syntax::{ShaderStage, InitDeclaratorList, Declaration,
    ExternalDeclaration, FullySpecifiedType, StorageQualifier,
    TypeQualifier, TypeQualifierSpec, TypeSpecifier, TypeSpecifierNonArray};

use std::fs::read_to_string;
use std::env;

#[derive(FromMeta)]
struct ShaderOpts {
    vertex: String,
    fragment: String,
}

#[derive(FromDeriveInput)]
struct ShaderStruct {
    #[darling(from_ident)]
    ident: syn::Ident,
    vis: syn::Visibility,
}

struct ShaderVar {
    ident: syn::Ident,
    ty: syn::Type,
    storageq: StorageQualifier,
    fn_str: &'static str,
}

#[proc_macro_attribute]
pub fn glue_shader_program(args: TokenStream, input: TokenStream) -> TokenStream {
    let res = input.clone();
    let attr_args = parse_macro_input!(args as AttributeArgs);
    let struct_args = parse_macro_input!(input as DeriveInput);

    let attr = match ShaderOpts::from_list(&attr_args) {
        Ok(v) => v,
        Err(e) => { return e.write_errors().into(); }
    };
    let inp = match ShaderStruct::from_derive_input(&struct_args) {
        Ok(v) => v,
        Err(e) => { return e.write_errors().into(); }
    };

    let err = format!("{} Make sure {} exists relative to {:?}\n",
                "Failed to open vertex shader file!", attr.vertex,
                env::current_dir().expect("No current working directory??"));
    let vert_string = read_to_string(attr.vertex)
        .expect(&err);
    let err = format!("{} Make sure {} exists relative to {:?}\n",
                "Failed to open fragment shader file!", attr.fragment,
                env::current_dir().expect("No current working directory??"));
    let frag_string = read_to_string(attr.fragment)
        .expect(&err);
    println!("Vert shader:\n{}", vert_string);
    println!("Frag shader:\n{}", vert_string);
    
    let mut vert_ast = ShaderStage::parse(vert_string)
        .expect("Vertex shader could not be compiled!");

    // Harvest the top-level declarations of attributes &c
    let mut tldecs = vec![];
    loop {
        let elt = vert_ast.pop();
        match elt {
            Some(s) => {
                if let ExternalDeclaration::Declaration(
                        Declaration::InitDeclaratorList(
                            InitDeclaratorList{head, tail}
                            )
                    ) = s {
                    assert!(tail.is_empty(), 
                            "This macro does not support multiple variable {}",
                            "declarations on the same line. PRs welcome!");
                    assert!(head.array_specifier.is_none(),
                            "Array attributes/uniforms/varyings not supported.");
                    let name = head.name
                        .expect("Top level variables must be named");
                    tldecs.push((head.ty, name));
                }
            },
            None => break,
        };
    }
    println!("\n\n### TOP LEVEL ELTS ###");
    println!("{:?}", tldecs);
    // Translate GLSL types into Rust types, and map GLSL idents to Rust idents.
    // We use NAlgebra types for this; the fully qualified path is to avoid
    // shenanigans.
    let mut vars = vec![];
    for (FullySpecifiedType {qualifier, ty}, name) in tldecs {
        let TypeQualifier{mut qualifiers} = qualifier
            .expect("Top level GLSL variables must have storage qualifiers");
        let st_qualifier = loop {
            let q = qualifiers.0.pop();
            println!("saw {:?} in {}", q, name);
            if let Some(TypeQualifierSpec::Storage(target)) = q {
                break target;
            } else if q == None {
                panic!("Top level GLSL variables should have a {}",
                       "storage qualifier, like attribute or uniform");
            }
        };
        let (ty, fn_str) = get_rust_type_from_glsl(&ty);
        vars.push(ShaderVar {
            ident: syn::Ident::new(name.as_str(), 
                           proc_macro::Span::call_site().into()),
            ty: ty,
            storageq: st_qualifier, 
            fnval: format_ident!("{}", fn_str,);
        });
    }

    res
}

fn get_rust_type_from_glsl(glsl_ty: &TypeSpecifier) -> (syn::Type, &'static str) {
    assert!(glsl_ty.array_specifier.is_none(),
            "This macro currently does not support arrays.");
    use TypeSpecifierNonArray::*;
    let (ty_str, fn_str) = match glsl_ty.ty {
        Int => (quote!(i32), "1i"),
        UInt => {
            println!("NOTE: this shader needs webgl2 due to a uint variable");
            (quote!(u32), "1u")
        },
        Float => (quote!(f32), "1f"),
        Vec2 => (quote!(::nalgebra::base::Vector2<f32>), "2f"),
        Vec3 => (quote!(::nalgebra::base::Vector3<f32>), "3f"),
        Vec4 => (quote!(::nalgebra::base::Vector4<f32>), "4f"),
        Mat2 => (quote!(::nalgebra::base::Matrix2<f32>), "matrix2fv"),
        Mat3 => (quote!(::nalgebra::base::Matrix3<f32>), "matrix3fv"),
        Mat4 => (quote!(::nalgebra::base::Matrix4<f32>), "matrix4fv"),
        _ => panic!("No mapping for type {:?} to a Rust/NAlgebra type!",
                    glsl_ty.ty)
    };
    /*
    let msg = "Failed to parse hardcoded type path. You should never see this";
    let path = syn::parse_str(ty_str).map(syn::Path::parse_mod_style)
        .expect(msg).expect(msg); 
    //  ^^^^^^^^^^^^ error flattening with foreign types is hard, ok
    syn::Type::Path(syn::TypePath { qself: None, path: path })
    */
    (syn::Type::Verbatim(ty_str), fn_str)
}
